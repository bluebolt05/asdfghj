## Flipkart auto buy bot - (delete old bot folder completely and redownload this)

## (Optional step - do this only if notifcations are needed)
1. Create account at https://pushover.net/ and then create API token with any name,description at https://pushover.net/apps/build
2. Install pushover app from playstore/app store in mobile and login.
3. Open .env file in notepad and Add "Your User Key" from https://pushover.net/ in PO_USER and the API token in PO_TOKEN in config.env
4. To test if its working, it should send "Program Started" message when you run docker-compose up flipkartbot
5. Make sure you set "Battery Optimization" for pushover app to "Dont Optimize", otherwise you may miss notifcations

6. See images below : 
7. PO_USER https://bitbucket.org/ixxykarter/asdfghj/downloads/Screenshot_2021-01-30_at_4.20.21_PM.png
8. https://bitbucket.org/ixxykarter/asdfghj/downloads/Screenshot_2021-01-30_at_4.22.01_PM.png
9. PO_TOKEN https://bitbucket.org/ixxykarter/asdfghj/downloads/Screenshot_2021-01-30_at_4.22.23_PM.png

## Setup

1. Install docker https://www.docker.com/products/docker-desktop
2. Change .env and run the commands below in command prompt.make sure you are in the downloaded directory in command prompt 
3. docker build --rm --tag flipkartbot .
4. docker-compose up flipkartbot
5. When stock arrives ACCEPT PAYMENT will be displayed and there will be a UPI request
6. Logs are in logs folder but you can also see it in the docker desktop app -> Containers.
7. Ctrl + C to stop the bot




